﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Page.aspx.cs" Inherits="final_project.Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h3 runat="server" id="mypagetitle"></h3>
    <p runat="server" id="mypagecontent"></p>
    <asp:SqlDataSource ID="page_select" runat="server" ConnectionString="<%$ ConnectionStrings:final_sql_con %>"></asp:SqlDataSource>

    <a href ="Edit_page.aspx?pageid=<% Response.Write(this.pageid); %>">Edit</a>

    <br />

    <asp:SqlDataSource runat="server" ID="del_page" connectionString="<%$ connectionStrings:final_sql_con %>"></asp:SqlDataSource>
    <asp:Button runat="server" Text="Delete" OnClick="DeletePage" />
</asp:Content>
