﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace final_project
{
    public partial class menu : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack) {
                return;
            }
           
            pages_select.SelectCommand = "select * from pages";
            DataView myview = (DataView)pages_select.Select(DataSourceSelectArguments.Empty);
            string menuitems = "";
            foreach (DataRowView row in myview)
            {
                string id = row["pageid"].ToString();
                string content = row["pagetitle"].ToString();
                menuitems += "<li><a href=\"Page.aspx?pageid=" + id + "\">" + content + "</a></li>";


            }
            menucontainer.InnerHtml = menuitems;
        }
    }
}