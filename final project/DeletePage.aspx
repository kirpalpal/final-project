﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DeletePage.aspx.cs" Inherits="final_project.DeletePage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <asp:SqlDataSource runat="server" ID="delete_page" ConnectionString="<%$ connectionStrings:final_sql_con %>"></asp:SqlDataSource>
    <asp:button runat="server" onclick="DeletePage" Text="Delete"/>

    <div runat="server" id="deleteRow" class="querybox"></div>

</asp:Content>
