﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace final_project
{
    public partial class Page : System.Web.UI.Page
    {

        public string pageid
        {
            get { return Request.QueryString["pageid"]; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //mypagetitle.InnerHtml = "THe page title im looking has an id of " + pageid;

            string sqlquery = "select * from Pages where pageid="+pageid;

            //mypagetitle.InnerHtml = sqlquery;
            page_select.SelectCommand = sqlquery;

            DataView pageview = (DataView)page_select.Select(DataSourceSelectArguments.Empty);
            DataRowView pagerowview = pageview[0];

            mypagetitle.InnerHtml = pagerowview["pagetitle"].ToString();
            mypagecontent.InnerHtml = pagerowview["pagecontent"].ToString();
            

        }

        protected void DeletePage(object sender, EventArgs e)
        {
            string query = "delete from pages where pageid = " + pageid;
            del_page.DeleteCommand = query;
            del_page.Delete();

        }
    }
}