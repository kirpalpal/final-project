﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Addpage.aspx.cs" Inherits="final_project.Addpage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h3 runat="server" id="add_new_page"></h3>


    <asp:SqlDataSource runat="server" ID="add_page" ConnectionString="<%$ connectionStrings:final_sql_con %>"></asp:SqlDataSource>
    <asp:SqlDataSource runat="server" ID="select_new_data" ConnectionString="<%$ connectionStrings:final_sql_con %>"></asp:SqlDataSource>

            <div class="insertRow">
                <label>Page Title</label>
                <asp:Textbox ID="title_page" runat="server"></asp:Textbox>
                    
            </div>
            <div class="insertRow">
                <label>Page Content</label>
                <asp:Textbox ID="contentPage" runat="server"></asp:Textbox>
                    </div>
           <asp:Button runat="server" Onclick="new_add_Page" Text="Click to Add" />
            <div runat="server" id="output" class="querybox"></div>

</asp:Content>
