﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace final_project
{
    public partial class Edit_page : System.Web.UI.Page

    {
        public int pageid
        {
            get { return Convert.ToInt32(Request.QueryString["pageid"]);}
            
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected override void OnPreRender(EventArgs e)
        {
            string query = "select * from pages where pageid =" + pageid;
            Page_select.SelectCommand = query;

            DataTable pageview = ((DataView)Page_select.Select(DataSourceSelectArguments.Empty)).ToTable();

            DataRow pagerowview = pageview.Rows[0];
            title_page.Text = pagerowview["pagetitle"].ToString();
            contentPage.Text = pagerowview["pagecontent"].ToString();
            
        }

        protected void editpage(object sender, EventArgs e)
        {
            string title = title_page.Text;
            string content = contentPage.Text;

            string query = "update Pages set pagetitle ='" + title + "',pagecontent='" + content + "'where pageid=" + pageid;
            output.InnerHtml = query;

            ed_page.UpdateCommand = query;
            ed_page.Update();
        }
    } 
}