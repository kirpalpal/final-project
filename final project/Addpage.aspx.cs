﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace final_project
{
    public partial class Addpage : System.Web.UI.Page
    {
        private string addquery = "Insert into Pages"+"(pagetitle,"+"pagecontent)values";

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void new_add_Page(object sender, EventArgs e)
        {
            string title = title_page.Text;
            string content = contentPage.Text;

            addquery += "('" + title + "','" + content + "')";
            output.InnerHtml = addquery;

            add_page.InsertCommand = addquery;
            add_page.Insert();

        }
    }
}