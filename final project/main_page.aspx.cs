﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace final_project
{
    public partial class main_page : System.Web.UI.Page
    {
        private string sqlline = "SELECT pageid,pagetitle,pagecontent from Pages";

        protected void Page_Load(object sender, EventArgs e)
        {
            page_sel.SelectCommand = sqlline;
            page_query.InnerHtml = sqlline;
            list_page.DataSource = main_Manual_Bind(page_sel);
            list_page.DataBind();


            ButtonColumn remove_page = new ButtonColumn();
            remove_page.HeaderText = "Delete";
            remove_page.Text = "Delete";
            remove_page.ButtonType = ButtonColumnType.PushButton;
            remove_page.CommandName = "del_page";
            list_page.Columns.Add(remove_page);
            list_page.DataBind();
        }
        protected void delete_ItemCommand(object sender, DataGridCommandEventArgs e)
        {
            if (e.CommandName == "del_page")
            {
                int pageid = Convert.ToInt32(e.Item.Cells[1].Text);
                page_del(pageid);
            
            }

        }

        protected void page_del(int pageid)
        {
            string deletequrey = "Delete FROM Pages WHERE pageid=" + pageid.ToString();
            del_page.DeleteCommand = deletequrey;
            del_page.Delete();
            page_query.InnerHtml = deletequrey;
        }

        protected DataView main_Manual_Bind(SqlDataSource src)
        {

            DataTable first_table;
            DataView first_view;
            first_table = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            foreach (DataRow row in first_table.Rows)
            {
                
               row["pagetitle"] ="<a href=\"Page.aspx?pageid="+row["pageid"]+"\">"+row["pagetitle"]+"</a>";

            }

            first_view = first_table.DefaultView;

            return first_view;
        }
    }
}
