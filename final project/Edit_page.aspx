﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Edit_page.aspx.cs" Inherits="final_project.Edit_page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h3 runat="server" id="edit_page"></h3>
    <asp:SqlDataSource runat="server" ID="ed_page" ConnectionString="<%$ connectionStrings:final_sql_con %>"></asp:SqlDataSource>
        <asp:SqlDataSource runat="server" ID="Page_select" ConnectionString="<%$ connectionStrings:final_sql_con %>"></asp:SqlDataSource>

            <div class="insertRow">
                <label>Page Title</label>
                <asp:Textbox ID="title_page" runat="server"></asp:Textbox>
                    
            </div>
            <div class="insertRow">
                <label>Page Content</label>
                <asp:Textbox ID="contentPage" runat="server"></asp:Textbox>
                    </div>
            <asp:button runat="server" Onclick="editpage" text="Edit Page" />
            <div runat="server" id="output" class="querybox"></div>


</asp:Content>
