﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="main_page.aspx.cs" Inherits="final_project.main_page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br />

    <div id="new_data" runat="server">
        <a href="Addpage">Add Data</a></div>
    <br />
    

    <asp:SqlDataSource runat="server" ID="page_sel" selectcommand="select * from pages"
        connectionString="<%$ connectionStrings:final_sql_con %>"></asp:SqlDataSource>
    <div id="page_query" class="query_input" runat="server"></div>
     
    <asp:DataGrid runat="server" ID="list_page" OnItemCommand ="delete_ItemCommand" ></asp:DataGrid>

    <asp:SqlDataSource runat="server" ID="del_page" connectionString="<%$ connectionStrings:final_sql_con %>"></asp:SqlDataSource>
</asp:Content>
